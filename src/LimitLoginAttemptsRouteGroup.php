<?php
namespace DKZR\LimitLoginAttempts\Slim;

use \Psr\Http\Message\ServerRequestInterface  as Request;
use \Psr\Http\Message\ResponseInterface       as Response;
use \Slim\App                                 as SlimApp;
use \Slim\Container                           as SlimContainer;
use \Slim\Exception\SlimException;

class LimitLoginAttemptsRouteGroup {
  protected $container;
  protected $settings;

  public function __construct( SlimContainer $container ) {
    $this->container = $container;
    $this->settings = ( $this->container->has( 'limit_login_attempts' ) ? $this->container->get( 'limit_login_attempts' ) : [] );
  }

  public function __invoke( SlimApp $app ) {
    // TODO: use $this->settings['setup_link'], when set
    $app->get( '', [ $this, 'getSetup' ] );

    $app->get( 'acl[/]', [ $this, 'getAcl' ] );
    $app->post( 'acl[/]', [ $this, 'postAcl' ] );
    $app->post( 'acl/create[/]', [ $this, 'postAcl' ] );
    $app->post( 'acl/delete[/]', [ $this, 'postAcl' ] );

    $app->get( 'lockout[/]', [ $this, 'getAcl' ] );
    $app->post( 'lockout[/]', [ $this, 'postAcl' ] );
    $app->post( 'lockout/delete[/]', [ $this, 'postAcl' ] );

    $app->get( 'country[/]', [ $this, 'getCountry' ] );
    $app->post( 'country/rule[/]', [ $this, 'postCountryRule' ] );
    $app->post( 'country/add[/]', [ $this, 'postCountryAdd' ] );
    $app->post( 'country/remove[/]', [ $this, 'postCountryRemove' ] );

    $app->get( 'log[/]', [ $this, 'getLog' ] );
    $app->get( 'stats[/]', [ $this, 'getStats' ] );
  }

  protected function allowed( Request $request, Response $response ) {
    $localhost_ips = $this->settings['localhost_ips'] ?? [];
    $ip = $_SERVER[ $this->container->has( 'server_remote_addr_key' ) ? $this->container->get( 'server_remote_addr_key' ) : 'REMOTE_ADDR' ];

    if ( empty( $localhost_ips ) || ! in_array( $ip, $localhost_ips ) ) {
      $response = $response->withJson( [ 'message' => 'Invalid IP' ], 401 );
      throw new SlimException( $request, $response );
    }
  }

  protected function log( string $method, Request $request, Response $response, $args ) {
    $params = $request->getParams();
    //error_log( print_r( compact( 'method', 'request', 'response', 'args', 'params' ), 1 ) );
    error_log( print_r( compact( 'method', 'args', 'params' ), 1 ) );
  }

  public function getSetup( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $api_uri = $request->getUri();

    $output = [
      'name'        => 'DKZR',
      'description' => 'Limit Login Attemts backend for sites hosted on DKZR system.',
      'id'          => '0',                                       // ID of the client
      'key'         => 'no-key',                                  // secret client API key
      'header'      => 'key',                                     // header field for client API key
      'version'     => '1',                                       // API version
      'api'         => sprintf( '%s/%s', $api_uri->getBaseUrl(), trim( $api_uri->getPath(), '/' ) ),
      'schema'      => '',
      'messages'    => [
        'setup_success'   => 'You have successfully connected to the DKZR backend.',
        'sync_error'      => '',
      ],
      'settings'    => [],                          // additional settings
    ];
    return $response->withJson( $output );
  }

  public function getAcl( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'count'   => 0,
      'offset'  => null,
      'items'   => [
        //[
        //  'rule'    => 'deny',
        //  'pattern' => '',
        //  'origin'  => 'auto',
        //],
      ],
    ];
    return $response->withJson( $output );
  }

  public function postAcl( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'result'        => 'allow',
      'reason'        => '',
      'attempts_left' => 3,
      'time_left'     => 0,
    ];
    return $response->withJson( $output );
  }

  public function postAclCreate( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'message' => 'Success',
    ];
    return $response->withJson( $output );
  }

  public function postAclDelete( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'message' => 'Success',
    ];
    return $response->withJson( $output );
  }

  public function getLockout( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'count'   => 0,
      'offset'  => null,
      'items'   => [
        //[
        //  'ip'    => '',
        //  'login' => [],
        //  'count' => 0,
        //  'ttl'   => 0,
        //],
      ],
    ];
    return $response->withJson( $output );
  }

  public function postLockout( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'result'        => 'allow',
      'reason'        => '',
      'attempts_left' => 3,
      'time_left'     => 0,
    ];
    return $response->withJson( $output );
  }

  public function postLockoutDelete( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'message' => 'Success',
    ];
    return $response->withJson( $output );
  }

  public function getCountry( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'count'   => 0,
      'offset'  => null,
      'items'   => [
        //[
        //  'ip'    => '',
        //  'login' => [],
        //  'count' => 0,
        //  'ttl'   => 0,
        //],
      ],
    ];
    return $response->withJson( $output );
  }

  public function postCountryRule( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'message' => 'Success',
    ];
    return $response->withJson( $output );
  }

  public function postCountryAdd( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'message' => 'Success',
    ];
    return $response->withJson( $output );
  }

  public function postCountryRemove( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'message' => 'Success',
    ];
    return $response->withJson( $output );
  }

  public function getLog( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'count'   => 0,
      'offset'  => null,
      'items'   => [
        //[
        //  'ip' => '',
        //  'login' => '',
        //  'pattern' => null,
        //  'result'  => 'deny',
        //  'reason'  => 'ip_lockout_allow_below_limit',
        //  'attempts_left' => 3,
        //  'time_left' => null,
        //  'created_at'  => 0,
        //  'actions' => [
        //    'method'  => 'unlock',
        //    'label' => 'Unlock IP',
        //    'icon'  => 'unlock',
        //    'color' => 'crimson',
        //    'data'  => [
        //      'ip'  => '',
        //    ],
        //  ],
        //],
      ],
      
    ];
    return $response->withJson( $output );
  }

  public function getStats( Request $request, Response $response, $args ) {
    $this->log( __METHOD__, $request, $response, $args );
    $this->allowed( $request, $response );

    $output = [
      'attempts' => [
        'at'    => [],
        'count' => [],
      ],
    ];
    return $response->withJson( $output );
  }
}
